#!/usr/bin/env bash

# Script to fetch latest commits for localisation files used by icecat
# Copyright (C) 2022 Alfred Wingate
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# TODO:
# * Could do with some refactoring


set -e

trap `[[ -n ${temp_dir} ]] && rm -rf ${temp_dir}` EXIT

temp_dir=$(mktemp -d)

BASE_URI="https://hg.mozilla.org/l10n-central/"

L10N="
ace ach af ak an ar as ast az be bg bn bn-BD bn-IN bo br brx bs ca ca-valencia cak ckb crh cs csb
cy da de dsb el en-CA en-GB en-ZA eo es-AR es-CL es-ES es-MX et eu fa ff fi fr frp fur fy-NL ga-IE
gd gl gn gu-IN gv he hi-IN hr hsb hto hu hy-AM hye ia id ilo is it ixl ja ja-JP-mac ka kab kk km kn
ko kok ks ku lb lg lij lo lt ltg lv mai meh mix mk ml mn mr ms my nb-NO ne-NP nl nn-NO nr nso ny oc
or pa-IN pai pbb pl pt-BR pt-PT rm ro ru rw sah sat sc scn sco si sk sl son sq sr ss st
sv-SE sw szl ta ta-LK te tg th tl tn tr trs ts tsz uk ur uz ve vi wo xcl xh zam zh-CN zh-TW zu
"

output=""

pushd ${temp_dir} > /dev/null

for lang in ${L10N}; do
	wget --quiet "${BASE_URI}${lang}/atom-log" -O "${lang}-atom.log"
	commits=$(sed -n "/<entry>/,/<id>/ s|<id>${BASE_URI}${lang}/#changeset-\([a-z0-9]*\)<\/id>|\1| p" "${lang}-atom.log")
	commit=$(echo ${commits} | awk '{print $1}')
	date=$(sed -n "/<id>.*${commit}<\/id>/,/<updated>/ s/<updated>\([0-9-]*\)T[0-9:+]*<\/updated>/\1/ p" "${lang}-atom.log")
	output+="LANG_COMMIT[${lang}]=\"${commit}\" # ${date}\n"
done
popd > /dev/null

printf "${output}"
