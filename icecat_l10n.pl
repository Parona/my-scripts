#!/usr/bin/env perl
use diagnostics;
use strict;
use warnings;

use AnyEvent::Strict;
use AnyEvent;
use LWP::Simple;
use Try::Tiny;
use XML::LibXML::XPathContext;
use XML::LibXML;

my $base_uri = 'https://hg.mozilla.org/l10n-central/';
my @L10N     = qw(
  ace ach af ak an ar as ast az be bg bn bn-BD
  bn-IN bo br brx bs ca ca-valencia cak ckb crh cs
  csb cy da de dsb el en-CA en-GB en-ZA eo es-AR
  es-CL es-ES es-MX et eu fa ff fi fr frp fur fy-NL
  ga-IE gd gl gn gu-IN gv he hi-IN hr hsb hto hu
  hy-AM hye ia id ilo is it ixl ja ja-JP-mac ka kab
  kk km kn ko kok ks ku lb lg lij lo lt ltg lv
  mai meh mix mk ml mn mr ms my nb-NO ne-NP nl
  nn-NO nr nso ny oc or pa-IN pai pbb pl pt-BR pt-PT
  rm ro ru rw sah sat sc scn sco si sk sl son sq
  sr ss st sv-SE sw szl ta ta-LK te tg th tl tn
  tr trs ts tsz uk ur uz ve vi wo xcl xh zam
  zh-CN zh-TW zu
);

my $namespace = XML::LibXML::XPathContext->new();
$namespace->registerNs( atom => 'http://www.w3.org/2005/Atom' );

my %async;

for my $lang (@L10N) {
    my $rec = {};
    $async{$lang} = $rec;

    my $pid = fork or exit 1;

    $rec->{'return'}  = AnyEvent->condvar;
    $rec->{'watcher'} = AnyEvent->child(
        pid => $pid,
        cb  => sub {
            my $output = get("${base_uri}${lang}/atom-log");
            if ( not defined $output ) {
                print "${base_uri}${lang} couldn't resolve";
                exit;
            }

            try {
                my $dom = XML::LibXML->load_xml( string => $output );
                my $entry =
                  ${ $namespace->findnodes( '//atom:entry', $dom ) }[0];

                ( my $commit ) =
                  ( $namespace->findvalue( './atom:id', $entry ) =~
                      /\#changeset-([[:alnum:]]*)/sxm );
                ( my $date ) =
                  ( $namespace->findvalue( './atom:updated', $entry ) =~
                      /^(\d{4}-\d{2}-\d{2})/sxm );
                $rec->{'return'}
                  ->send("LANG_COMMIT[${lang}]=\"${commit}\" # ${date}\n");
            }
            catch {
                print "Problem with $lang: $_";
                exit 1;
            };
        }
    );
}

for (@L10N) {
    print $async{$_}{'return'}->recv;
}
